#ifndef PROGRAM_SFA_HPP
#define PROGRAM_SFA_HPP

#include "../DataStructure/SmartFixedArray/SmartFixedArray.hpp"

class Program_SFA
{
    public:
    void Run();

    private:
    SmartFixedArray<int> m_data;
};

#endif
