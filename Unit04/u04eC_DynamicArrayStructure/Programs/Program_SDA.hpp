#ifndef PROGRAM_SDA_HPP
#define PROGRAM_SDA_HPP

#include "../DataStructure/SmartDynamicArray/SmartDynamicArray.hpp"

class Program_SDA
{
    public:
    void Run();

    private:
    SmartDynamicArray<int> m_data;
};

#endif
