#include "DataStructure/SmartDynamicArray/SmartDynamicArrayTester.hpp"

#include "Utilities/Logger.hpp"
#include "Utilities/Menu.hpp"

#include "Programs/Program_SDA.hpp"

int main()
{
    Logger::Setup();

    bool done = false;
    while ( !done )
    {
        Menu::Header( "Data Structures" );

        int choice = Menu::ShowIntMenuWithPrompt( {
            "Run tests",
            "Run program",
            "Exit"
        } );

        switch( choice )
        {
            case 1:
            {
                SmartDynamicArrayTester sdaTester;
                sdaTester.Start();
            }
            break;

            case 2:
            {
                Program_SDA menu;
                menu.Run();
            }
            break;

            case 3:
                done = true;
            break;
        }
    }

    Logger::Cleanup();

    return 0;
}
