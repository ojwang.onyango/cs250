#include "CsvParser.hpp"

#include <iostream>
using namespace std;

//struct CsvDocument
//{
//    vector<string> header;
//    vector< vector< string > > rows;
//};

CsvDocument CsvParser::Parse( string filepath )
{
    ifstream input( filepath );
    if ( input.fail() )
    {
        cout << "ERROR: Couldn't open \"" << filepath << "\"!" << endl;
        throw runtime_error( "File not found!" );
    }

    CsvDocument doc;

    // First row should be header
    string strHeader;
    getline( input, strHeader );

    // Split up by comma
    doc.header = StringUtil::Split( strHeader, "," );

    // Get remaining data
    string row;
    int rowCount = 0;
    while ( getline( input, row ) )
    {
        doc.rows.push_back( StringUtil::CsvSplit( row, ',' ) );
    }

    input.close();

    return doc;
}
