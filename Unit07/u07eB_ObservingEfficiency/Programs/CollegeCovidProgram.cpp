#include "CollegeCovidProgram.hpp"

#include "../Utilities/Menu.hpp"
#include "../Utilities/CsvParser.hpp"
#include "../Utilities/Timer.hpp"

#include "../Sorting/BubbleSort/BubbleSort.hpp"
#include "../Sorting/InsertionSort/InsertionSort.hpp"
#include "../Sorting/MergeSort/MergeSort.hpp"
#include "../Sorting/QuickSort/QuickSort.hpp"
#include "../Sorting/SelectionSort/SelectionSort.hpp"

#include <iostream>
using namespace std;

CollegeCovidProgram::~CollegeCovidProgram()
{
    Clean( m_collegeData );
}

void CollegeCovidProgram::Run()
{
    Init();

    while ( !m_done )
    {
        Menu::Header( "COLLEGES AND COVID :(" );
        int choice = Menu::ShowIntMenuWithPrompt( {
            "Quit to main menu",
            "Search data",
            "Sort data",
            "View all data (" + StringUtil::ToString( m_collegeData.size() ) + " records)"
        } );

        switch( choice )
        {
            case 1:     m_done = true;      break;
            case 2:     Menu_Search();      break;
            case 3:     Menu_Sort();        break;
            case 4:     ViewAllData();      break;
            default:    cout << "Invalid selection!" << endl;
        }
    }
}

void CollegeCovidProgram::ViewAllData()
{
    Menu::Header( "VIEW ALL DATA" );
    for ( auto& college : m_collegeData )
    {
        cout << college << endl;
    }
}

void CollegeCovidProgram::Menu_Search()
{
    enum class SearchCriteria {
        STATE=1, COUNTY=2, CITY=3, COLLEGE=4, GTCASES=5, LTCASES=6
    };

    Menu::Header( "SEARCH DATA" );

    cout << "Search via which field?" << endl;
    SearchCriteria searchBy = static_cast<SearchCriteria>(Menu::ShowIntMenuWithPrompt( {
        "state",
        "county",
        "city",
        "college",
        "cases >= x",
        "cases <= x"
    } ));

    string strCriteria;
    int numCriteria;

    // Get their search criteria
    switch( searchBy )
    {
        case SearchCriteria::STATE:
        case SearchCriteria::COUNTY:
        case SearchCriteria::CITY:
        case SearchCriteria::COLLEGE:
        cout << "Enter criteria: ";
        cin.ignore();
        getline( cin, strCriteria );
        break;

        case SearchCriteria::GTCASES:
        case SearchCriteria::LTCASES:
        cout << "Enter integer value: ";
        cin >> numCriteria;
        break;
    }

    // Collect the data
    Timer timer;
    timer.Start();
    vector<College*> results;
    for ( const auto& college : m_collegeData )
    {
        if      (
                ( searchBy == SearchCriteria::STATE && StringUtil::Contains( college->state, strCriteria, false ) )             ||
                ( searchBy == SearchCriteria::COUNTY && StringUtil::Contains( college->county, strCriteria, false ) )           ||
                ( searchBy == SearchCriteria::CITY && StringUtil::Contains( college->city, strCriteria, false ) )               ||
                ( searchBy == SearchCriteria::COLLEGE && ( StringUtil::Contains( college->college, strCriteria, false ) ) )     ||
                ( searchBy == SearchCriteria::GTCASES && college->cases >= numCriteria )                                 ||
                ( searchBy == SearchCriteria::LTCASES && college->cases <= numCriteria )
                )
        {
            results.push_back( college );
        }
   }

   Menu::Header( StringUtil::ToString( results.size() ) + " RESULTS" );

   cout << "Finished in " << timer.GetElapsedMilliseconds() << " milliseconds" << endl << endl;

   for ( const auto& result : results )
   {
        cout << *result << endl;
   }
}

void CollegeCovidProgram::Menu_Sort()
{
    Menu::Header( "SORT DATA" );

    cout << endl << "MERGE SORT...      " << endl;
    {
        cout << "* Cloning dataset... " << endl;
        vector<College*> dataCopy = Duplicate( m_collegeData );
        Timer timer;
        cout << "* Beginning sort..." << endl;
        timer.Start();
        MergeSort::Sort( dataCopy );
        cout << "* Sorted " << dataCopy.size() << " records in " << timer.GetElapsedMilliseconds() << " milliseconds" << endl;
        Clean( dataCopy );
    }

    cout << endl << "QUICK SORT...      " << endl;
    {
        cout << "* Cloning dataset... " << endl;
        vector<College*> dataCopy = Duplicate( m_collegeData );
        Timer timer;
        cout << "* Beginning sort..." << endl;
        timer.Start();
        QuickSort::Sort( dataCopy );
        cout << "* Sorted " << dataCopy.size() << " records in " << timer.GetElapsedMilliseconds() << " milliseconds" << endl;
        Clean( dataCopy );
    }

    cout << endl << "SELECTION SORT...  " << endl;
    {
        cout << "* Cloning dataset... " << endl;
        vector<College*> dataCopy = Duplicate( m_collegeData );
        Timer timer;
        cout << "* Beginning sort..." << endl;
        timer.Start();
        SelectionSort::Sort( dataCopy );
        cout << "* Sorted " << dataCopy.size() << " records in " << timer.GetElapsedMilliseconds() << " milliseconds" << endl;
        Clean( dataCopy );
    }

    cout << endl << "INSERTION SORT...  " << endl;
    {
        cout << "* Cloning dataset... " << endl;
        vector<College*> dataCopy = Duplicate( m_collegeData );
        Timer timer;
        cout << "* Beginning sort..." << endl;
        timer.Start();
        InsertionSort::Sort( dataCopy );
        cout << "* Sorted " << dataCopy.size() << " records in " << timer.GetElapsedMilliseconds() << " milliseconds" << endl;
        Clean( dataCopy );
    }

    cout << endl << "BUBBLE SORT...     " << endl;
    {
        cout << "* Cloning dataset... " << endl;
        vector<College*> dataCopy = Duplicate( m_collegeData );
        Timer timer;
        cout << "* Beginning sort..." << endl;
        timer.Start();
        BubbleSort::Sort( dataCopy );
        cout << "* Sorted " << dataCopy.size() << " records in " << timer.GetElapsedMilliseconds() << " milliseconds" << endl;
        Clean( dataCopy );
    }

    cout << endl << endl;
}

vector<College*> CollegeCovidProgram::Duplicate( const vector<College*> original )
{
    vector<College*> clone;

    for ( const auto& ptr : original )
    {
        // Allocate new memory and copy the data over
        College* newObj = new College;
        *newObj = *ptr;
        clone.push_back( newObj );
    }

    return clone;
}

void CollegeCovidProgram::Init()
{
    m_done = false;

    Timer timer;
    cout << "Loading in data now... ";

    timer.Start();
    CsvDocument doc = CsvParser::Parse( "../data/colleges.csv" );
    cout << " Finished loading " << doc.rows.size() << " records in " << timer.GetElapsedMilliseconds() << " milliseconds" << endl;

    cout << "Converting data... ";
    timer.Start();
    // Convert doc data to colleges
    int counter = 0;
    for ( const auto& row : doc.rows )
    {
        College* newCollege = new College;
        newCollege->Set( doc.header, row );
        m_collegeData.push_back( newCollege );
        counter++;
    }

    cout << " Finished converting in " << timer.GetElapsedMilliseconds() << " milliseconds" << endl;
}

void CollegeCovidProgram::Clean( vector<College*> cleanme )
{
    for ( size_t i = 0; i < cleanme.size(); i++ )
    {
        if ( cleanme[i] != nullptr )
        {
            delete cleanme[i];
            cleanme[i] = nullptr;
        }
    }
}
