#include "VetProgram.hpp"

#include "../Utilities/Menu.hpp"
#include "../Utilities/CsvParser.hpp"
#include "../Utilities/Timer.hpp"

#include "../Sorting/BubbleSort/BubbleSort.hpp"
#include "../Sorting/InsertionSort/InsertionSort.hpp"
#include "../Sorting/MergeSort/MergeSort.hpp"
#include "../Sorting/QuickSort/QuickSort.hpp"
#include "../Sorting/SelectionSort/SelectionSort.hpp"

#include <iostream>
using namespace std;

VetProgram::~VetProgram()
{
    Clean( m_allData );
}

void VetProgram::Run()
{
    Init();

    while ( !m_done )
    {
        Menu::Header( "VET INTAKE" );
        int choice = Menu::ShowIntMenuWithPrompt( {
            "Quit to main menu",
            "Search data",
            "Sort data",
            "View all data (" + StringUtil::ToString( m_allData.size() ) + " records)"
        } );

        switch( choice )
        {
            case 1:     m_done = true;      break;
            case 2:     Menu_Search();      break;
            case 3:     Menu_Sort();        break;
            case 4:     ViewAllData();      break;
            default:    cout << "Invalid selection!" << endl;
        }
    }
}

void VetProgram::ViewAllData()
{
    Menu::Header( "VIEW ALL DATA" );
    for ( auto& record : m_allData )
    {
        cout << record << endl;
    }
}

void VetProgram::Menu_Search()
{
    enum class SearchCriteria {
        ANIMALID=1, INTAKETYPE=2, ANIMALTYPE=3
    };

    Menu::Header( "SEARCH DATA" );

    cout << "Search via which field?" << endl;
    SearchCriteria searchBy = static_cast<SearchCriteria>(Menu::ShowIntMenuWithPrompt( {
        "animal ID",
        "intake type",
        "animal type",
    } ));

    string strCriteria;
    cout << "Enter criteria: ";
    cin.ignore();
    getline( cin, strCriteria );


    // Collect the data
    Timer timer;
    timer.Start();
    vector<AnimalIntake*> results;
    for ( const auto& record : m_allData )
    {
        if      (
                ( searchBy == SearchCriteria::ANIMALID && StringUtil::Contains( record->AnimalID, strCriteria, false ) )             ||
                ( searchBy == SearchCriteria::INTAKETYPE && StringUtil::Contains( record->IntakeType, strCriteria, false ) )           ||
                ( searchBy == SearchCriteria::ANIMALTYPE && StringUtil::Contains( record->AnimalType, strCriteria, false ) )
                )
        {
            results.push_back( record );
        }
   }

   Menu::Header( StringUtil::ToString( results.size() ) + " RESULTS" );

   cout << "Finished in " << timer.GetElapsedMilliseconds() << " milliseconds" << endl << endl;

   for ( const auto& result : results )
   {
        cout << *result << endl;
   }
}

void VetProgram::Menu_Sort()
{
    Menu::Header( "SORT DATA" );

    cout << "This might take some time! If it takes over a few minutes, you might go into" << endl;
    cout << "the [data/Animal_Services_Intake_Data.csv] file and remove half the records," << endl;
    cout << "and keep doing that until it doesn't take forever!!" << endl << endl;

    Menu::DrawHorizontalBar( 80, '.' );

    cout << endl << "MERGE SORT...      " << endl;
    cout.flush();
    {
        cout << "* Cloning dataset... " << endl;
        vector<AnimalIntake*> dataCopy = Duplicate( m_allData );
        Timer timer;
        cout << "* Beginning sort..." << endl;
        timer.Start();
        MergeSort::Sort( dataCopy );
        cout << "* Sorted " << dataCopy.size() << " records in " << timer.GetElapsedMilliseconds() << " milliseconds" << endl;
        Clean( dataCopy );
    }

    cout << endl << "QUICK SORT...      " << endl;
    cout.flush();
    {
        cout << "* Cloning dataset... " << endl;
        vector<AnimalIntake*> dataCopy = Duplicate( m_allData );
        Timer timer;
        cout << "* Beginning sort..." << endl;
        timer.Start();
        QuickSort::Sort( dataCopy );
        cout << "* Sorted " << dataCopy.size() << " records in " << timer.GetElapsedMilliseconds() << " milliseconds" << endl;
        Clean( dataCopy );
    }

    cout << endl << "SELECTION SORT...  " << endl;
    cout.flush();
    {
        cout << "* Cloning dataset... " << endl;
        vector<AnimalIntake*> dataCopy = Duplicate( m_allData );
        Timer timer;
        cout << "* Beginning sort..." << endl;
        timer.Start();
        SelectionSort::Sort( dataCopy );
        cout << "* Sorted " << dataCopy.size() << " records in " << timer.GetElapsedMilliseconds() << " milliseconds" << endl;
        Clean( dataCopy );
    }

    cout << endl << "INSERTION SORT...  " << endl;
    cout.flush();
    {
        cout << "* Cloning dataset... " << endl;
        vector<AnimalIntake*> dataCopy = Duplicate( m_allData );
        Timer timer;
        cout << "* Beginning sort..." << endl;
        timer.Start();
        InsertionSort::Sort( dataCopy );
        cout << "* Sorted " << dataCopy.size() << " records in " << timer.GetElapsedMilliseconds() << " milliseconds" << endl;
        Clean( dataCopy );
    }

    cout << endl << "BUBBLE SORT...     " << endl;
    cout.flush();
    {
        cout << "* Cloning dataset... " << endl;
        vector<AnimalIntake*> dataCopy = Duplicate( m_allData );
        Timer timer;
        cout << "* Beginning sort..." << endl;
        timer.Start();
        BubbleSort::Sort( dataCopy );
        cout << "* Sorted " << dataCopy.size() << " records in " << timer.GetElapsedMilliseconds() << " milliseconds" << endl;
        Clean( dataCopy );
    }

    cout << endl << endl;
}

vector<AnimalIntake*> VetProgram::Duplicate( const vector<AnimalIntake*> original )
{
    vector<AnimalIntake*> clone;

    for ( const auto& ptr : original )
    {
        // Allocate new memory and copy the data over
        AnimalIntake* newObj = new AnimalIntake;
        *newObj = *ptr;
        clone.push_back( newObj );
    }

    return clone;
}

void VetProgram::Init()
{
    m_done = false;

    Timer timer;
    cout << "Loading in data now... ";

    timer.Start();
    CsvDocument doc = CsvParser::Parse( "../data/Animal_Services_Intake_Data.csv" );
    cout << " Finished loading " << doc.rows.size() << " records in " << timer.GetElapsedMilliseconds() << " milliseconds" << endl;

    cout << "Converting data... ";
    timer.Start();
    // Convert doc data to colleges
    int counter = 0;
    for ( const auto& row : doc.rows )
    {
        AnimalIntake* record = new AnimalIntake;
        record->Set( doc.header, row );
        m_allData.push_back( record );
        counter++;
    }

    cout << " Finished converting in " << timer.GetElapsedMilliseconds() << " milliseconds" << endl;
}

void VetProgram::Clean( vector<AnimalIntake*> cleanme )
{
    for ( size_t i = 0; i < cleanme.size(); i++ )
    {
        if ( cleanme[i] != nullptr )
        {
            delete cleanme[i];
            cleanme[i] = nullptr;
        }
    }
}
