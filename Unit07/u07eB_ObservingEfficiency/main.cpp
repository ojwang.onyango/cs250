#include "Utilities/Menu.hpp"
#include "Utilities/CsvParser.hpp"

#include "Sorting/BubbleSort/BubbleSortTester.hpp"
#include "Sorting/InsertionSort/InsertionSortTester.hpp"
#include "Sorting/MergeSort/MergeSortTester.hpp"
#include "Sorting/QuickSort/QuickSortTester.hpp"
#include "Sorting/SelectionSort/SelectionSortTester.hpp"

#include "Programs/CollegeCovidProgram.hpp"
#include "Programs/VetProgram.hpp"

void RunTests();

int main()
{
    bool done = false;
    while ( !done )
    {
        Menu::Header( "MAIN MENU" );
        int choice = Menu::ShowIntMenuWithPrompt( { "Quit", "Run tests", "College covid data", "Vet data" } );

        switch( choice )
        {
            case 1:     done = true;        break;
            case 2:     RunTests();         break;
            case 3:
            {
                CollegeCovidProgram ccp;
                ccp.Run();
            }
            break;
            case 4:
            {
                VetProgram vp;
                vp.Run();
            }
            break;
            default:    cout << "Invalid input" << endl;
        }
    }

    return 0;
}

void RunTests()
{
    BubbleSortTester bsTester;
    bsTester.Start();

    InsertionSortTester isTester;
    isTester.Start();

    MergeSortTester msTester;
    msTester.Start();

    QuickSortTester qsTester;
    qsTester.Start();

    SelectionSortTester ssTester;
    ssTester.Start();
}





