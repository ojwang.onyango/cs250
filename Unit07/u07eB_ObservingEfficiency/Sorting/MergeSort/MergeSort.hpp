#ifndef _MERGE_SORT_HPP
#define _MERGE_SORT_HPP

#include "../../Exceptions/NotImplementedException.hpp"

#include <algorithm>    // includes swap()
using namespace std;

namespace MergeSort
{

// Declarations
template <typename T>
void Sort( vector<T>& arr );

template <typename T>
void Sort( vector<T>& arr, int left, int right );

template <typename T>
void Merge( vector<T>& arr, int left, int mid, int right );

// Definitions
template <typename T>
void Sort( vector<T>& arr )
{
//    throw NotImplementedException( "MergeSort" );
    Sort( arr, 0, arr.size() - 1 );
}

template <typename T>
void Sort( vector<T>& arr, int left, int right )
{
    if ( left < right )
    {
        int mid = ( left + right ) / 2;

        Sort( arr, left, mid );
        Sort( arr, mid+1, right );
        Merge( arr, left, mid, right );
    }
}

template <typename T>
void Merge( vector<T>& arr, int left, int mid, int right )
{
    const int n1 = mid - left + 1;
    const int n2 = right - mid;

    vector<T> leftVec;
    vector<T> rightVec;

    for ( int i = 0; i < n1; i++ )
    {
        leftVec.push_back( arr[left + i] );
    }

    for ( int j = 0; j < n2; j++ )
    {
        rightVec.push_back( arr[mid + 1 + j] );
    }

    int i = 0;
    int j = 0;
    int k = left;

    while ( i < n1 && j < n2 )
    {
        if ( leftVec[i] <= rightVec[j] )
        {
            arr[k] = leftVec[i];
            i++;
        }
        else
        {
            arr[k] = rightVec[j];
            j++;
        }
        k++;
    }

    while ( i < n1 )
    {
        arr[k] = leftVec[i];
        i++;
        k++;
    }

    while ( j < n2 )
    {
        arr[k] = rightVec[j];
        j++;
        k++;
    }
}

}

#endif
