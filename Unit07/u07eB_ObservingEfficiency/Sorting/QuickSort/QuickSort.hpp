#ifndef _QUICK_SORT_HPP
#define _QUICK_SORT_HPP

#include "../../Exceptions/NotImplementedException.hpp"

#include <algorithm>    // includes swap()
using namespace std;

namespace QuickSort
{

// Declarations
template <typename T>
void Sort( vector<T>& arr );

template <typename T>
void Sort( vector<T>& arr, int low, int high );

template <typename T>
int Partition( vector<T>& arr, int low, int high );

// Definitions
template <typename T>
void Sort( vector<T>& arr )
{
    Sort( arr, 0, arr.size() - 1 );
}

template <typename T>
void Sort( vector<T>& arr, int low, int high )
{
//    throw NotImplementedException( "QuickSort" );
    if ( low < high )
    {
        int partIndex = Partition( arr, low, high );
        Sort( arr, low, partIndex - 1 );
        Sort( arr, partIndex + 1, high );
    }
}

template <typename T>
int Partition( vector<T>& arr, int low, int high )
{
    T pivotValue = arr[high];
    int i = low - 1;

    for ( int j = low; j <= high - 1; j++ )
    {
        if ( arr[j] <= pivotValue )
        {
            i++;
            swap( arr[i], arr[j] );
        }
    }

    swap( arr[i+1], arr[high] );
    return i + 1;
}

}

#endif
