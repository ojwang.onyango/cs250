#include "DataStructure/BinarySearchTree/BinarySearchTreeTester.hpp"

#include "Utilities/Logger.hpp"
#include "Utilities/Menu.hpp"

#include "Programs/Program_BST.hpp"

void DataStructuresTests();
void DataStructuresPrograms();

int main()
{
    Logger::Setup();

    bool done = false;
    while ( !done )
    {
        Menu::Header( "Aggregated CS250 Topics" );

        int choice = Menu::ShowIntMenuWithPrompt( {
            "Exit",
            "Data structures - run tests",
            "Data structures - run programs",
        } );

        switch( choice )
        {
            case 1:     done = true;                break;

            case 2:     DataStructuresTests();      break;

            case 3:     DataStructuresPrograms();   break;

            default:    cout << ":|" << endl;
        }
    }

    Logger::Cleanup();

    return 0;
}

void DataStructuresTests()
{
    BinarySearchTreeTester bstTester;
    bstTester.Start();
}

void DataStructuresPrograms()
{
    Program_BST menu_BST;
    menu_BST.Run();
}









