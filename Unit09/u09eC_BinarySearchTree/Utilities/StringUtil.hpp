#ifndef _STRINGS
#define _STRINGS

#include <string>
#include <sstream>
#include <vector>
using namespace std;

class StringUtil
{
    public:
    template <typename T>
    static string ToString( const T& value );
    static string BoolToText( const bool value );
    static int StringToInt( const string& str );
    static string ToUpper( const string& val );
    static string ToLower( const string& val );
    static string ColumnText( int colWidth, const string& text );
    static bool Contains( string haystack, string needle, bool caseSensitive = true );
    static int Find( string haystack, string needle );
    static string Replace( string fulltext, string findme, string replacewith );
    static vector<string> Split( string str, string delim );
    static vector<string> CsvSplit( string str, char delim );
};

template <typename T>
string StringUtil::ToString( const T& value )
{
    stringstream ss;
    ss << value;
    return ss.str();
}

#endif
