#include "DataStructure/LinkedList/LinkedListTester.hpp"
#include "DataStructure/SmartDynamicArray/SmartDynamicArrayTester.hpp"
#include "DataStructure/Queue/QueueTester.hpp"
#include "DataStructure/Stack/StackTester.hpp"

#include "Utilities/Logger.hpp"
#include "Utilities/Menu.hpp"

#include "Programs/Program_Stack.hpp"
#include "Programs/Program_Queue.hpp"

int main()
{
    Logger::Setup();

    bool done = false;
    while ( !done )
    {
        Menu::Header( "Data Structures" );

        int choice = Menu::ShowIntMenuWithPrompt( {
            "Run tests",
            "Run program",
            "Exit"
        } );

        switch( choice )
        {
            case 1:
            {
                SmartDynamicArrayTester sdaTester;
                sdaTester.Start();

                LinkedListTester llTester;
                llTester.Start();

                QueueTester qTester;
                qTester.Start();

                StackTester sTester;
                sTester.Start();
            }
            break;

            case 2:
            {
                Program_Stack menu_S;
                menu_S.Run();

                Program_Queue menu_Q;
                menu_Q.Run();
            }
            break;

            case 3:
                done = true;
            break;
        }
    }

    Logger::Cleanup();

    return 0;
}
