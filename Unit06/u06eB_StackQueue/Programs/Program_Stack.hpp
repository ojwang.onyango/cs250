#ifndef PROGRAM_S_HPP
#define PROGRAM_S_HPP

#include "../DataStructure/Stack/ArrayStack.hpp"
#include "../DataStructure/Stack/LinkedStack.hpp"

class Program_Stack
{
    public:
    void Run();

    private:
    ArrayStack<int> m_dataArr;
    LinkedStack<int> m_dataLink;
};

#endif
