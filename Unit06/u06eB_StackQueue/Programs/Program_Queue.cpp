#include "Program_Queue.hpp"

#include "../Utilities/Menu.hpp"
#include "../Utilities/StringUtil.hpp"
#include "../Utilities/Logger.hpp"

#include "../Exceptions/InvalidIndexException.hpp"
#include "../Exceptions/ItemNotFoundException.hpp"
#include "../Exceptions/NotImplementedException.hpp"
#include "../Exceptions/NullptrException.hpp"
#include "../Exceptions/StructureEmptyException.hpp"

#include <cstdlib>
#include <ctime>

void Program_Queue::Run()
{
    srand( time( NULL ) );

    ofstream eventLog( "eventlog-queue.txt" );

    for ( int i = 0; i < 1000; i++ )
    {
        int random = rand() % 3;

        try
        {
            switch( random )
            {
                case 0:     // Push
                {
                    int randomValue = rand() % 100;
                    eventLog << "Push( " << randomValue << " )" << endl;
                    cout << "Push( " << randomValue << " )" << endl;
                    m_dataArr.Push( randomValue );
                    m_dataLink.Push( randomValue );
                } break;
                case 1:     // Pop
                {
                    if ( m_dataArr.IsEmpty() ) { continue; }
                    eventLog << "Pop from queue" << endl;
                    cout << "Pop from queue" << endl;
                    m_dataArr.Pop();
                    m_dataLink.Pop();
                } break;
                case 2:    // Front
                {
                    eventLog << "Get front item: " << m_dataArr.Front() << endl;
                    cout << "Get front item: " << m_dataArr.Front() << endl;
                } break;
            }
        }
        catch( const NotImplementedException& ex )
        {
            eventLog << "EXCEPTION: " << ex.what() << endl;
            cout << "EXCEPTION: " << ex.what() << endl;
        }
        catch( const InvalidIndexException& ex )
        {
            eventLog << "EXCEPTION: " << ex.what() << endl;
            cout << "EXCEPTION: " << ex.what() << endl;
        }
        catch( const StructureEmptyException& ex )
        {
            eventLog << "EXCEPTION: " << ex.what() << endl;
            cout << "EXCEPTION: " << ex.what() << endl;
        }
        catch( const NullptrException& ex )
        {
            eventLog << "EXCEPTION: " << ex.what() << endl;
            cout << "EXCEPTION: " << ex.what() << endl;
        }
        catch( ... )
        {
            eventLog << "EXCEPTION: Misc" << endl;
            cout << "EXCEPTION: Misc" << endl;
        }
    }

    eventLog << "Clearing out array queue..." << endl;
    cout << "Clearing out array queue..." << endl;
    while ( !m_dataArr.IsEmpty() )
    {
        eventLog << "Pop item " << m_dataArr.Front() << endl;
        cout << "Pop item " << m_dataArr.Front() << endl;
        m_dataArr.Pop();
    }

    eventLog << "Clearing out linked queue..." << endl;
    cout << "Clearing out linked queue..." << endl;
    while ( !m_dataLink.IsEmpty() )
    {
        eventLog << "Pop item " << m_dataLink.Front() << endl;
        cout << "Pop item " << m_dataLink.Front() << endl;
        m_dataLink.Pop();
    }

    eventLog.close();
}

