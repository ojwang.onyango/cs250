#ifndef PROGRAM_Q_HPP
#define PROGRAM_Q_HPP

#include "../DataStructure/Queue/ArrayQueue.hpp"
#include "../DataStructure/Queue/LinkedQueue.hpp"

class Program_Queue
{
    public:
    void Run();

    private:
    ArrayQueue<int> m_dataArr;
    LinkedQueue<int> m_dataLink;
};

#endif
