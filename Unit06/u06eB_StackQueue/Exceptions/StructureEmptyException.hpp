#ifndef _STRUCTURE_EMPTY_EXCEPTION
#define _STRUCTURE_EMPTY_EXCEPTION

#include <stdexcept>
#include <string>
using namespace std;

class StructureEmptyException : public runtime_error
{
    public:
    StructureEmptyException( string functionName, string message )
        : runtime_error( "[" + functionName + "] " + message  ) { ; }
};

#endif
