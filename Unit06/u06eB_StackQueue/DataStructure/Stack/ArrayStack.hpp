#ifndef ARRAY_STACK_HPP
#define ARRAY_STACK_HPP

#include "../SmartDynamicArray/SmartDynamicArray.hpp"
#include "../../Utilities/Logger.hpp"
#include "../../Exceptions/NotImplementedException.hpp"
#include "../../Exceptions/StructureFullException.hpp"
#include "../../Exceptions/InvalidIndexException.hpp"
#include "../../Exceptions/NullptrException.hpp"

template <typename T>
//! A last-in-first-out (LIFO) stack structure built on top of an array
class ArrayStack
{
    public:
    //! Push a new item into the back of the queue
    void Push(const T& newData );
    //! Remove the item at the front of the queue
    void Pop() noexcept;
    //! Access the data at the front of the queue
    T Top() const;
    //! Get the amount of items in the queue
    int Size();
    //! Return whether the queue is empty
    bool IsEmpty();

    private:
    SmartDynamicArray<T> m_vector;

    friend class StackTester;
};

template <typename T>
void ArrayStack<T>::Push( const T& newData )
{
    throw NotImplementedException( "ArrayStack::Push" );
}

template <typename T>
void ArrayStack<T>::Pop() noexcept
{
    throw NotImplementedException( "ArrayStack::Pop" );
}

template <typename T>
T ArrayStack<T>::Top() const
{
    throw NotImplementedException( "ArrayStack::Front" );
}

template <typename T>
int ArrayStack<T>::Size()
{
    throw NotImplementedException( "ArrayStack::Size" );
}

template <typename T>
bool ArrayStack<T>::IsEmpty()
{
    throw NotImplementedException( "ArrayStack::IsEmpty" );
}

#endif
