#include "Program1.h"
#include "Program2.h"
#include "Program3.h"

#include "utilities/Menu.hpp"

#include <iostream>
#include <iomanip>
#include <array>
#include <string>
using namespace std;

// Don't modify main
int main()
{
    bool done = false;

    while ( !done )
    {
        Menu::Header( "MAIN MENU" );
        string choice = Menu::ShowStringMenuWithPrompt( { "EXIT", "Price lookup", "File reader", "Pizza distributor" } );

        if      ( choice == "EXIT" )                { done = true; }
        else if ( choice == "Price lookup" )        { Program1(); }
        else if ( choice == "File reader" )         { Program2(); }
        else if ( choice == "Pizza distributor" )   { Program3(); }
    }

    return 0;
}
