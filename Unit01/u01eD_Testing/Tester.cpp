#include "Tester.hpp"
#include "Room.hpp"
#include <iostream>
using namespace std;

void RoomTester::Run()
{
    Test_SetWidth();
    /*Test_SetLength();
    Test_GetWidth();
    Test_GetLength();
    Test_Sqft();*/
}

void RoomTester::Test_SetWidth()
{
    cout << endl << "----------------- Test_SetWidth -----------------" << endl;
    
    Room room;
    float input;
    float expected, actual;

    /* TEST 1 ****************************************************/
    cout << endl << "TEST 1... ";

    // Set up variables
    input = 100;
    expected = 100;

    // Run function to test
    room.SetWidth(input);

    // Get the data to make sure it was set
    actual = room.m_width;

    // Compare results
    if (actual == expected) { cout << "PASS" << endl; }
    else { cout << "FAIL" << endl; }

    // Display variable values:
    cout << "input:     " << input << endl;
    cout << "expected:  " << expected << endl;
    cout << "actual:    " << actual << endl;

    /* TEST 2 ****************************************************/
    cout << endl << "TEST 2... ";

    // Set up variables
    input = 25;
    expected = 25;

    // Run function to test
    room.SetWidth(input);

    // Get the data to make sure it was set
    actual = room.m_width;

    // Compare results
    if (actual == expected) { cout << "PASS" << endl; }
    else { cout << "FAIL" << endl; }

    // Display variable values:
    cout << "input:     " << input << endl;
    cout << "expected:  " << expected << endl;
    cout << "actual:    " << actual << endl;

    /* TEST 3 ****************************************************/
    cout << endl << "TEST 3... ";

    // Set up variables
    input = -100;
    expected = 5;
    room.m_width = 5;

    // Run function to test
    room.SetWidth(input);

    // Get the data to make sure it was set
    actual = room.m_width;

    // Compare results
    if (actual == expected) { cout << "PASS" << endl; }
    else { cout << "FAIL" << endl; }

    // Display variable values:
    cout << "input:     " << input << endl;
    cout << "expected:  " << expected << endl;
    cout << "actual:    " << actual << endl;
}

void RoomTester::Test_SetLength()
{
    cout << endl << "----------------- Test_SetLength -----------------" << endl;

    Room room;
    float input;
    float expected, actual;

    /* TEST 1 ****************************************************/
    cout << endl << "TEST 1... ";

    // Set up variables
    input = 100;
    expected = 100;

    // Run function to test
    room.SetLength( input );

    // Get the data to make sure it was set
    actual = room.m_length;

    // Compare results
    if ( actual == expected )       { cout << "PASS" << endl; }
    else                            { cout << "FAIL" << endl; }

    // Display variable values:
    cout << "input:     " << input << endl;
    cout << "expected:  " << expected << endl;
    cout << "actual:    " << actual << endl;
}

void RoomTester::Test_GetWidth()
{
    cout << endl << "----------------- Test_GetWidth -----------------" << endl;

    Room room;
    float expected, actual;

    /* TEST 1 ****************************************************/
    cout << endl << "TEST 1... ";

    // Set up variables
    room.m_width = 100;
    expected = 100;

    // Get the data to make sure it was set
    actual = room.GetWidth();

    // Compare results
    if ( actual == expected )       { cout << "PASS" << endl; }
    else                            { cout << "FAIL" << endl; }

    // Display variable values:
    cout << "room.m_width:  " << room.m_width << endl;
    cout << "expected:      " << expected << endl;
    cout << "actual:        " << actual << endl;
}

void RoomTester::Test_GetLength()
{
    cout << endl << "----------------- Test_GetLength -----------------" << endl;

    Room room;
    float expected, actual;

    /* TEST 1 ****************************************************/
    cout << endl << "TEST 1... ";

    // Set up variables
    room.m_length = 100;
    expected = 100;

    // Get the data to make sure it was set
    actual = room.GetLength();

    // Compare results
    if ( actual == expected )       { cout << "PASS" << endl; }
    else                            { cout << "FAIL" << endl; }

    // Display variable values:
    cout << "room.m_length: " << room.m_length << endl;
    cout << "expected:      " << expected << endl;
    cout << "actual:        " << actual << endl;
}

void RoomTester::Test_Sqft()
{
    cout << endl << "----------------- Test_Sqft -----------------" << endl;

    Room room;
    float expected, actual;

    /* TEST 1 ****************************************************/
    cout << endl << "TEST 1... ";

    // Set up variables
    room.m_width = 40;
    room.m_length = 25;
    expected = 1000;

    // Get the data to make sure it was set
    actual = room.Sqft();

    // Compare results
    if ( actual == expected )       { cout << "PASS" << endl; }
    else                            { cout << "FAIL" << endl; }

    // Display variable values:
    cout << "room.m_width:     " << room.m_width << endl;
    cout << "room.m_length:    " << room.m_length << endl;
    cout << "expected:         " << expected << endl;
    cout << "actual:           " << actual << endl;
}
