#include "Room.hpp"
#include "Tester.hpp"

int main()
{
    RoomTester tester;
    tester.Run();

    return 0;
}
