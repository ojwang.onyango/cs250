#ifndef _TESTER_HPP
#define _TESTER_HPP

class RoomTester
{
    public:
    void Run();
    void Test_SetWidth();
    void Test_SetLength();
    void Test_GetWidth();
    void Test_GetLength();
    void Test_Sqft();
};

#endif
