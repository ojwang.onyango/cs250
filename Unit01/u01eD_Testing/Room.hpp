#ifndef _ROOM_HPP
#define _ROOM_HPP

class Room
{
    public:
    void SetWidth( float width );
    void SetLength( float length );

    float GetWidth();
    float GetLength();

    float Sqft();

    private:
    float m_width;
    float m_length;

    friend class RoomTester;
};

#endif
