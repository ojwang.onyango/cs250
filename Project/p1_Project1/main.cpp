#include "Program.h"

#include <array>
#include <iostream>
using namespace std;

int main()
{
    Program program;
    program.Run();

    return 0;
}
