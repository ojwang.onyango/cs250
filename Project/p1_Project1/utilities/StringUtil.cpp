#include "StringUtil.hpp"

#include <iostream>
#include <string>
#include <sstream>
#include <stdexcept>
using namespace std;

int StringUtil::StringToInt( const std::string& str )
{
    int value = 0;
    try
    {
        value = stoi( str );
    }
    catch( const std::invalid_argument& ex )
    {
        std::cout << "Cannot convert to int!" << std::endl;
        return 0;
    }

    return value;

//    istringstream ss( str );
//    int val;
//    ss >> val;
//    return val;
}

string StringUtil::ToUpper( const string& val )
{
    string upper = "";
    for ( unsigned int i = 0; i < val.size(); i++ )
    {
        upper += toupper( val[i] );
    }
    return upper;
}

string StringUtil::ToLower( const string& val )
{
    string upper = "";
    for ( unsigned int i = 0; i < val.size(); i++ )
    {
        upper += tolower( val[i] );
    }
    return upper;
}

string StringUtil::ColumnText( int colWidth, const string& text )
{
    string adjusted = text;
    for ( int i = 0; i < colWidth - text.size(); i++ )
    {
        adjusted += " ";
    }
    return adjusted;
}

bool StringUtil::Contains( string haystack, string needle )
{
    return ( haystack.find( needle ) != string::npos );
}

string StringUtil::Replace( string fulltext, string findme, string replacewith )
{
    int index = fulltext.find( findme );
    if ( index == string::npos ) { return fulltext; }

    string updated = fulltext.replace( index, findme.size(), replacewith );

    return updated;
}


