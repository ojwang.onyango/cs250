#include "Program.h"

#include "utilities/StringUtil.hpp"

#include <iomanip>
#include <iostream>
using namespace std;

Program::Program()
{
    string filename = "log.txt";
    m_log.open( filename );
    cout << "Saving program log to \"" << filename << "\"" << endl;
    m_time = 0;
}

Program::~Program()
{
    m_log.close();
}

void Program::Run()
{
    // Load event file
    string filepath = "../data/passengerLog.txt";
    ifstream input( filepath );
    if ( input.fail() )
    {
        cout << "Unable to open filepath \"" << filepath << "\"!" << endl;
        return;
    }

    // TODO: 1. Enqueue and board

    // TODO: 2. Disembark

    // This goes at the end of Run()
    cout << "Printing summary..." << endl;
    Summary();
    cout << "Done, view log file for all details" << endl;
}

void Program::LineUp( const Passenger& passenger )
{
    // TODO: Implement
}

void Program::Embark( string time )
{
    // TODO: Implement
}

void Program::Disembark( string time )
{
    // TODO: Implement
}

void Program::Summary() // done
{
    m_log << endl << "SUMMARY" << endl << string( 80, '-' ) << endl;

    m_log << left
        << setw( 10 ) << "TICKET"
        << setw( 30 ) << "NAME"
        << setw( 10 ) << "ENQUEUE"
        << setw( 10 ) << "BOARD"
        << setw( 10 ) << "ARRIVE"
        << setw( 10 ) << "SEAT"
        << endl << string( 80, '-' ) << endl;

//    for ( size_t i = 0; i < m_arrivedPassengers.size(); i++ )
//    {
//        m_log << left
//            << setw( 10 ) << m_arrivedPassengers[i].GetTicketNumber()
//            << setw( 30 ) << m_arrivedPassengers[i].GetName()
//            << setw( 10 ) << m_arrivedPassengers[i].GetEnqueueTime()
//            << setw( 10 ) << m_arrivedPassengers[i].GetBoardingTime()
//            << setw( 10 ) << m_arrivedPassengers[i].GetArrivalTime()
//            << setw( 10 ) << m_arrivedPassengers[i].GetSeat()
//            << endl;
//    }
}

string Program::NiceTime( int hr, int min ) // done
{
    string nice = StringUtil::ToString( hr ) + ":";
    if ( min < 10 )
    {
        nice += "0";
    }
    nice += StringUtil::ToString( min );
    return nice;
}

void Program::TimePasses( int& hr, float& min ) // done
{
    min += 0.05;
    if ( min >= 60 )
    {
        hr++;
        min = 0;
    }
}

