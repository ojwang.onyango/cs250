#include "DataStructure/BinarySearchTree/BinarySearchTreeTester.hpp"

#include "Utilities/Logger.hpp"
#include "Utilities/Menu.hpp"

#include "Programs/Program_BST.hpp"

#include "Projects/Project2/VetProgram.hpp"

void DataStructuresTests();
void DataStructuresPrograms();
void Projects();

int main()
{
    Logger::Setup();

    bool done = false;
    while ( !done )
    {
        Menu::Header( "CS 250 - Project 2 - Binary Search Trees" );

        int choice = Menu::ShowIntMenuWithPrompt( {
            "Exit",
            "Data structures - run tests",
            "Data structures - run programs",
            "Projects - run projects",
        } );

        switch( choice )
        {
            case 1:     done = true;                break;

            case 2:     DataStructuresTests();      break;

            case 3:     DataStructuresPrograms();   break;

            case 4:     Projects();                 break;

            default:    cout << " :| \n No." << endl;
        }
    }

    Logger::Cleanup();

    return 0;
}

void DataStructuresTests()
{
    BinarySearchTreeTester bstTester;
    bstTester.Start();
}

void DataStructuresPrograms()
{
    Program_BST menu_BST;
    menu_BST.Run();
}

void Projects()
{
    VetProgram program;
    program.Run();
}
