#include "StringUtil.hpp"

#include <iostream>
#include <string>
#include <sstream>
#include <stdexcept>
using namespace std;

int StringUtil::StringToInt( const std::string& str )
{
    int value = 0;
    try
    {
        value = stoi( str );
    }
    catch( const std::invalid_argument& ex )
    {
//        std::cout << "Cannot convert \"" << str << "\" to int!" << std::endl;
        return -1;
    }

    return value;

//    istringstream ss( str );
//    int val;
//    ss >> val;
//    return val;
}

float StringUtil::StringToFloat( const std::string& str )
{
    float value = 0;
    try
    {
        value = stof( str );
    }
    catch( const std::invalid_argument& ex )
    {
//        std::cout << "Cannot convert \"" << str << "\" to int!" << std::endl;
        return -1;
    }

    return value;
}

string StringUtil::ToUpper( const string& val )
{
    string upper = "";
    for ( unsigned int i = 0; i < val.size(); i++ )
    {
        upper += toupper( val[i] );
    }
    return upper;
}

string StringUtil::ToLower( const string& val )
{
    string upper = "";
    for ( unsigned int i = 0; i < val.size(); i++ )
    {
        upper += tolower( val[i] );
    }
    return upper;
}

string StringUtil::ColumnText( int colWidth, const string& text )
{
    string adjusted = text;
    for ( int i = 0; i < colWidth - text.size(); i++ )
    {
        adjusted += " ";
    }
    return adjusted;
}

bool StringUtil::Contains( string haystack, string needle, bool caseSensitive /* = true */ )
{
    string a = haystack;
    string b = needle;

    if ( !caseSensitive )
    {
        for ( unsigned int i = 0; i < a.size(); i++ )
        {
            a[i] = tolower( a[i] );
        }

        for ( unsigned int i = 0; i < b.size(); i++ )
        {
            b[i] = tolower( b[i] );
        }
    }

    return ( a.find( b ) != string::npos );
}

int StringUtil::Find( string haystack, string needle )
{
    return haystack.find( needle );
}

string StringUtil::Replace( string fulltext, string findme, string replacewith )
{
    string updated = fulltext;
    int index = updated.find( findme );

    while ( index != string::npos )
    {
        // Make the replacement
        updated = updated.replace( index, findme.size(), replacewith );

        // Look for the item again
        index = updated.find( findme );
    }

    return updated;
}

vector<string> StringUtil::Split( string str, string delim )
{
    vector<string> data;

    int begin = 0;
    int end = 0;

    while ( str.find( delim ) != string::npos )
    {
        end = str.find( delim );

        // Get substring up until delimiter
        int length = end - begin;
        string substring = str.substr( begin, length );
        data.push_back( substring );

        // Remove this chunk of string
        str = str.erase( begin, length+1 );
    }

    // Put last string in structure
    data.push_back( str );

    return data;
}

vector<string> StringUtil::CsvSplit( string str, char delim )
{
    vector<string> data;

    int begin = 0;

    bool singleQuote = false;
    bool doubleQuote = false;

    for ( unsigned int i = 0; i < str.size(); i++ )
    {

        if ( str[i] == '\'' )
        {
            singleQuote = !singleQuote;
        }
        else if ( str[i] == '"' )
        {
            doubleQuote = !doubleQuote;
        }
        else if ( str[i] == delim )
        {
            if ( singleQuote || doubleQuote )
            {
                // We're inside quotes, ignore this bastard.
            }
            else
            {
                // OK we wanna pull this data
                int length = i - begin;
                string substring = str.substr( begin, length );
                data.push_back( substring );
                begin = i+1;
            }
        }
    }

    // Pull the last chunk
    int length = str.size() - begin;
    string substring = str.substr( begin, length );
    data.push_back( substring );


    return data;
}

string StringUtil::BoolToText( const bool value )
{
    return ( value ) ? "TRUE" : "FALSE";
}

