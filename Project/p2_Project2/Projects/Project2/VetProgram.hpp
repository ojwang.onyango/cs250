#ifndef _VET_PROGRAM_HPP
#define _VET_PROGRAM_HPP

#include "AnimalIntake.hpp"

#include "../../DataStructure/BinarySearchTree/BinarySearchTree.hpp"

#include <string>
#include <fstream>
using namespace std;

class VetProgram
{
    public:
    VetProgram();
    ~VetProgram();

    void Run();
    void Menu_Search();
    void Menu_Add();
    void Menu_GetMinKey();
    void Menu_GetMaxKey();
    void Menu_GetHeight();

    private:
    void Init();

    bool m_done;
    BinarySearchTree<string, AnimalIntake*> m_allData;
    vector<AnimalIntake*> m_pointerLog; // this is so I can clean up the data
    ofstream m_metricsLog;
};

#endif
