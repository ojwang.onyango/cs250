#include "VetProgram.hpp"

#include "../../Utilities/Menu.hpp"
#include "../../Utilities/CsvParser.hpp"
#include "../../Utilities/Timer.hpp"

#include <iostream>
using namespace std;

VetProgram::VetProgram()
{
    // This file displays information on how long different operations take
    m_metricsLog.open( "metric.txt" );
}

VetProgram::~VetProgram()
{
    // Clean up all the dynamically allocated data ;-;
    for ( size_t i = 0; i < m_pointerLog.size(); i++ )
    {
        if ( m_pointerLog[i] != nullptr )
        {
            delete m_pointerLog[i];
            m_pointerLog[i] = nullptr;
        }
    }
    m_metricsLog.close();
}

void VetProgram::Run()
{
    Init();

    while ( !m_done )
    {
        Menu::Header( "VET INTAKE" );
        cout << " " << StringUtil::ToString( m_allData.GetCount() ) + " record(s)" << endl << endl;
        int choice = Menu::ShowIntMenuWithPrompt( {
            "Search by key",
            "Add data",
            "Get min key",
            "Get max key",
            "Get tree height"
        }, true, true );

        switch( choice )
        {
            case 0:     m_done = true;          break;
            case 1:     Menu_Search();          break;
            case 2:     Menu_Add();             break;
            case 3:     Menu_GetMinKey();       break;
            case 4:     Menu_GetMaxKey();       break;
            case 5:     Menu_GetHeight();       break;
            default:    cout << "Invalid selection!" << endl;
        }
    }
}

/**
This function lets the user select which data file to load in.
It parses the CSV file, converts it to AnimalIntake objects, and stores
the pointers in a vector so it can be cleaned up later.
You will also need to add each item to the Binary Search Tree m_allData.
*/
void VetProgram::Init()
{
    // Load which file?
    m_done = false;
    string filePath = "../Projects/Project2/data/";
    cout << "Load in which data set?" << endl;
    string fileName = Menu::ShowStringMenuWithPrompt( {
        "Intake_100.csv",
        "Intake_1000.csv",
        "Intake_10000.csv",
        "Intake_187594.csv"
    } );

    Timer timer;
    cout << "Loading in data now... ";

    // Read in the CSV Document
    timer.Start();
    CsvDocument doc = CsvParser::Parse( filePath + fileName );
    cout << " Finished loading " << doc.rows.size() << " records in " << timer.GetElapsedMilliseconds() << " milliseconds" << endl;

    cout << "Converting data, storing in STL Vector... ";
    timer.Start();

    // Convert doc data to colleges
    for ( const auto& row : doc.rows )
    {
        AnimalIntake* record = new AnimalIntake;
        record->Set( doc.header, row );
        m_pointerLog.push_back( record );
    }
    cout << " Finished converting in " << timer.GetElapsedMilliseconds() << " milliseconds" << endl;


    // Pushing data to the tree
    cout << " Pushing data to tree... ";
    timer.Start();
    int counter = 0;
    for ( auto* record : m_pointerLog )
    {
        cout << "Pushing item " << m_allData.GetCount() << "/" << doc.rows.size() << " (Key = " << record->AnimalID << ") into tree... ";
        m_metricsLog << "Pushing item " << m_allData.GetCount() << "/" << doc.rows.size() << " (Key = " << record->AnimalID << ") into tree... ";

        // "record" is a pointer to an AnimalIntake object we're going to put in the tree.
        // The tree is       BinarySearchTree<string, AnimalIntake*> m_allData;

        Timer pushTimer;
        pushTimer.Start();
        try
        {
            // TODO: Push the current record into the tree. Use the record's AnimalID as its key.                       !!!
        }
        catch( ... ) { /* Just ignore duplicate keys */ }

        cout << pushTimer.GetElapsedMilliseconds() << " milliseconds" << endl;
        m_metricsLog << pushTimer.GetElapsedMilliseconds() << " milliseconds" << endl;

        counter++;
    }
    cout << " Finished pushing in " << timer.GetElapsedMilliseconds() << " milliseconds" << endl;
}

/**
Allows the user to enter a key to search for in the tree. Searches for the item,
displays the record if found.
*/
void VetProgram::Menu_Search()
{
    Menu::Header( "SEARCH DATA" );

    cout << "Enter a key to search for: ";
    string key;
    cin >> key;

    Timer timer;
    cout << endl << "Searching for item with key \"" << key << "\"... ";
    m_metricsLog << endl << "Searching for item with key \"" << key << "\"... ";

    timer.Start();

    AnimalIntake* ptrResult = nullptr;

    // TODO: Use the GetData function to find the record with this key.                                                 !!!

    cout << "Finished in " << timer.GetElapsedMilliseconds() << " milliseconds" << endl << endl;
    m_metricsLog << "Finished in " << timer.GetElapsedMilliseconds() << " milliseconds" << endl << endl;

    if ( ptrResult == nullptr )
    {
        // The result will be nullptr if no matches were found.
        cout << endl << "NO MATCHES FOUND" << endl;
    }
    else
    {
        // If the result isn't nullptr, we can de-reference it to view the record.
        cout << *ptrResult << endl;
    }

    Menu::Pause();
}

/**
Allows the user to enter in new data for a new record.
Will then add the data to the tree.
*/
void VetProgram::Menu_Add()
{
    Menu::Header( "ADD RECORD" );

    // Creates a new record (on the heap) and gets the user to enter all its information:
    AnimalIntake* newRecord = new AnimalIntake;
    cin.ignore();
    cin >> *newRecord;

    cout << endl << " Pushing item (Key = " << newRecord->AnimalID << ") into tree... ";
    m_metricsLog << "Pushing item (Key = " << newRecord->AnimalID << ") into tree... ";
    Timer pushTimer;

    pushTimer.Start();
    try
    {
        // TODO: Push the new record to the tree                                                                        !!!
        // BinarySearchTree<string, AnimalIntake*> m_allData
        // Use the record's AnimalID as its key.
    }
    catch( ... )
    {
        cout << "ERROR: Key was a duplicate!" << endl;
    }
    cout << endl << " " << pushTimer.GetElapsedMilliseconds() << " milliseconds" << endl;
    m_metricsLog << pushTimer.GetElapsedMilliseconds() << " milliseconds" << endl;

    // Save the pointer information for cleanup later.
    m_pointerLog.push_back( newRecord );
}

/**
Call the GetMinKey function of the tree
BinarySearchTree<string, AnimalIntake*> m_allData
Then call GetData, passing in that key.
Store the returned record, and display it to the screen.
*/
void VetProgram::Menu_GetMinKey()
{
    Menu::Header( "MIN KEY" );
    // TODO: cout the min key and record with that key.                                                                 !!!
    Menu::Pause();
}

/**
Call the GetMaxKey function of the tree
BinarySearchTree<string, AnimalIntake*> m_allData
Then call GetData, passing in that key.
Store the returned record, and display it to the screen.
*/
void VetProgram::Menu_GetMaxKey()
{
    Menu::Header( "MAX KEY" );
    // TODO: cout the max key and record with that key.                                                                 !!!
    Menu::Pause();
}

/**
Call the GetHeight function of the tree
BinarySearchTree<string, AnimalIntake*> m_allData
and display the height to the screen.
*/
void VetProgram::Menu_GetHeight()
{
    Menu::Header( "HEIGHT" );
    // TODO: cout the height of the tree.                                                                               !!!
    Menu::Pause();
}

