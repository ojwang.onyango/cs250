#include "DataStructure/LinkedList/LinkedListTester.hpp"

#include "Utilities/Logger.hpp"
#include "Utilities/Menu.hpp"

#include "Programs/Program_LL.hpp"

int main()
{
    Logger::Setup();

    bool done = false;
    while ( !done )
    {
        Menu::Header( "Data Structures" );

        int choice = Menu::ShowIntMenuWithPrompt( {
            "Run tests",
            "Run program",
            "Exit"
        } );

        switch( choice )
        {
            case 1:
            {
                LinkedListTester llTester;
                llTester.Start();
            }
            break;

            case 2:
            {
                Program_LL menu_LL;
                menu_LL.Run();
            }
            break;

            case 3:
                done = true;
            break;
        }
    }

    Logger::Cleanup();

    return 0;
}
