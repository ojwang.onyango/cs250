#ifndef PROGRAM_LL_HPP
#define PROGRAM_LL_HPP

#include "../DataStructure/LinkedList/LinkedList.hpp"

class Program_LL
{
    public:
    void Run();

    private:
    LinkedList<int> m_data;
};

#endif
