#include "Program_LL.hpp"

#include "../Utilities/Menu.hpp"
#include "../Utilities/StringUtil.hpp"
#include "../Utilities/Logger.hpp"

#include "../Exceptions/InvalidIndexException.hpp"
#include "../Exceptions/ItemNotFoundException.hpp"
#include "../Exceptions/NotImplementedException.hpp"
#include "../Exceptions/NullptrException.hpp"
#include "../Exceptions/StructureEmptyException.hpp"

#include <cstdlib>
#include <ctime>

void Program_LL::Run()
{
    m_data.Clear();

    srand( time( NULL ) );

    ofstream eventLog( "eventlog.txt" );

    for ( int i = 0; i < 1000; i++ )
    {
        int random = rand() % 6;

        try
        {
            switch( random )
            {
                case 0:     // PushFront
                {
                    int randomValue = rand() % 100;
                    eventLog << "PushFront( " << randomValue << " )" << endl;
                    cout << "PushFront( " << randomValue << " )" << endl;
                    m_data.PushFront( randomValue );
                } break;
                case 1:     // PushBack
                {
                    int randomValue = rand() % 100;
                    eventLog << "PushBack( " << randomValue << " )" << endl;
                    cout << "PushBack( " << randomValue << " )" << endl;
                    m_data.PushBack( randomValue );
                } break;
                case 2:    // PushAt
                {
                    if ( m_data.Size() == 0 ) { continue; }
                    int randomValue = rand() % 100;
                    int randomIndex = rand() % m_data.Size();
                    eventLog << "PushBack( " << randomValue << ", " << randomIndex << " )" << endl;
                    cout << "PushBack( " << randomValue << ", " << randomIndex << " )" << endl;
                    m_data.PushAt( randomValue, randomIndex );
                } break;
                case 3:     // PopFront
                {
                    eventLog << "PopFront()" << endl;
                    cout << "PopFront()" << endl;
                    m_data.PopFront();
                } break;
                case 4:     // PopBack
                {
                    eventLog << "PopBack()" << endl;
                    cout << "PopBack()" << endl;
                    m_data.PopBack();
                } break;
                case 5:     // PopAt
                {
                    if ( m_data.Size() == 0 ) { continue; }
                    int randomIndex = rand() % m_data.Size();
                    eventLog << "PopAt( " << randomIndex << " )" << endl;
                    cout << "PopAt( " << randomIndex << " )" << endl;
                    m_data.PopAt( randomIndex );
                } break;
            }
        }
        catch( const NotImplementedException& ex )
        {
            eventLog << "EXCEPTION: " << ex.what() << endl;
            cout << "EXCEPTION: " << ex.what() << endl;
        }
        catch( const InvalidIndexException& ex )
        {
            eventLog << "EXCEPTION: " << ex.what() << endl;
            cout << "EXCEPTION: " << ex.what() << endl;
        }
        catch( const StructureEmptyException& ex )
        {
            eventLog << "EXCEPTION: " << ex.what() << endl;
            cout << "EXCEPTION: " << ex.what() << endl;
        }
        catch( const NullptrException& ex )
        {
            eventLog << "EXCEPTION: " << ex.what() << endl;
            cout << "EXCEPTION: " << ex.what() << endl;
        }
        catch( ... )
        {
            eventLog << "EXCEPTION: Misc" << endl;
            cout << "EXCEPTION: Misc" << endl;
        }
    }

    m_data.Display( eventLog );
    m_data.Display( cout );

    eventLog.close();
}

