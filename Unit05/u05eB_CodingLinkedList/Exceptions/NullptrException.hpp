#ifndef _NULLPTR_EXCEPTION
#define _NULLPTR_EXCEPTION

#include <stdexcept>
#include <string>
using namespace std;

class NullptrException : public runtime_error
{
    public:
    NullptrException( string functionName, string message )
        : runtime_error( "[" + functionName + "] " + message ) { ; }
};

#endif
