#ifndef _INVALID_INDEX_EXCEPTION
#define _INVALID_INDEX_EXCEPTION

#include <stdexcept>
#include <string>
using namespace std;

class InvalidIndexException : public runtime_error
{
    public:
    InvalidIndexException( string functionName, string message )
        : runtime_error( "[" + functionName + "] " + message  ) { ; }
};

#endif
